# You will need to do the following first before you run ansible-pull

**your user account needs to have proper permissions, for homebrew**

`sudo chown -R $(whoami):wheel /usr/local/`

**if you don't have hosts file for ansible, you will need one**

`sudo mkdir -p "/etc/ansible" && touch "/etc/ansible/hosts"`

**run ansible-pull command**

`ansible-pull -f -C "master" -U https://gitlab.com/divemarkus/ansible-pull-macos.git ansible-pull-macos.yml -i "localhost"`

**how to use homebrew**

*brew help* will show you the list of commands that are available

*brew list* will show you the list of installed packages. You can also append formulae, for example brew list postgres will tell you of files installed by postgres (providing it is indeed installed)

*brew search* <search term> will list the possible packages that you can install. brew search post will return multiple packages that are available to install that have post in their name

*brew info* <package name> will display some basic information about the package in question

**Deploy notes:**

* virtualbox is throwing errors, so I removed from cask
* rm -rf ~/.ansible/pull/*
* need to write powerline to bash_profile
* had to re-run ansible-pull a few times
* your hosts file needs to only have the hostname
* never perform sudo for homebrew install
* troubleshoot virtualbox

**What worked**

Cask:
* iterm2
* macvim
* royal-tsx
* spotify

Package:
* ag (the_silver_searcher)
* autoconf
* bash-completion
* htop
* neofetch (added to bash_profile as well)
* nmap
* powerline-go (does not work. remove this!)
* tmux
* tree
* vault
* wget
* zsh-syntax-highlighting


**Default shell**

Depending on which default shell you use, the original lineinfile works for default. But if using zsh, perform the following:

1.  Create a file: 
     vi ~/.zshrc
2.  Add your config, like:
    neofetch
    powerline

**More powerline-go info**
* Kinda helpful links: 

```
  https://github.com/justjanne/powerline-go
  https://gist.github.com/wm/4750511
  https://sunlightmedia.org/bash-vs-zsh/
  https://nickolaskraus.org/articles/my-local-development-setup-2018/
  https://medium.com/@KentaKodashima/customize-your-terminal-prompt-with-powerline-ed6eb884fabb
  https://medium.com/@ITZDERR/how-to-install-powerline-to-pimp-your-bash-prompt-for-mac-9b82b03b1c02
  https://www.freecodecamp.org/news/how-to-configure-your-macos-terminal-with-zsh-like-a-pro-c0ab3f3c1156/
```

* This is a working .zshrc file:


```
function powerline_precmd() {
    eval "$(/usr/local/bin/powerline-go -error $? -shell zsh -eval -modules-right git)"
}

function install_powerline_precmd() {
  for s in "${precmd_functions[@]}"; do
    if [ "$s" = "powerline_precmd" ]; then
      return
    fi
  done
  precmd_functions+=(powerline_precmd)
}

if [ "$TERM" != "linux" ]; then
    install_powerline_precmd
fi
```

